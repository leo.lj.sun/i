<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Sun
 * Date: 13-10-7
 * Time: 下午1:59
 * To change this template use File | Settings | File Templates.
 */
namespace i\Helper;
class View {

    /**
     * 引用库里面的js文件
     * 优先引用项目内的
     * 没有的话再引用框架级别的
     * @param $source
     */
    public static function js($source){
        $jsPath = $_SERVER['SERVER_NAME'].strtr($_SERVER['SCRIPT_NAME'],array('index.php'=>'')).'Public/js/';
        $file = $jsPath.$source;
        echo '<script type="text/javascript" charset="utf-8" src="http://'.$file.'"></script>';
        return;
    }

    public static function css($source){
        $cssPath = $_SERVER['SERVER_NAME'].strtr($_SERVER['SCRIPT_NAME'],array('index.php'=>'')).'Public/css/';
        $file = $cssPath.$source;
        echo '<link rel="stylesheet" charset="utf-8" type="text/css" href="http://'.$file.'"/>';
        return;
    }

    /**
     * 引用umeditor
     * 使用此方法，不需要再引用jquery
     * editor.getContent()  //若编辑器中只包含字符"<p><br /></p/>"会返回空。
     * editor.getContent(function(){ return false //编辑器没有内容 ，getContent直接返回空 })
     * editor.getAllHtml() 取得完整的html代码，可以直接显示成完整的html文档
     * editor.getPlainTxt()得到编辑器的纯文本内容，但会保留段落格式
     * editor.getContentTxt() 获取编辑器中的纯文本内容,没有段落格式
     */
    public static function umeditor(){
        self::js('jQuery/jquery-1.10.2.js');
        $path = $_SERVER['SERVER_NAME'].strtr($_SERVER['SCRIPT_NAME'],array('index.php'=>'')).'Public/';
        echo '<link rel="stylesheet" charset="utf-8" type="text/css" href="http://'.$path.'js/ueditor/themes/default/css/umeditor.css"/>';
        self::js('ueditor/umeditor.config.js');
        self::js('ueditor/umeditor.js');
        return;
    }

    /**
     * 引用bootstrap,此方法包含jquery
     */
    public static function bootstrap(){
        $path = $_SERVER['SERVER_NAME'].strtr($_SERVER['SCRIPT_NAME'],array('index.php'=>'')).'Public/';
        echo '<script type="text/javascript" charset="utf-8" src="http://'.$path.'js/jQuery/jquery-1.7.1.js"></script>';
        echo '<script type="text/javascript" charset="utf-8" src="http://'.$path.'bootstrap/js/bootstrap.min.js"></script>';
        echo '<link rel="stylesheet" charset="utf-8" type="text/css" href="http://'.$path.'bootstrap/css/bootstrap.min.css"/>';
        echo '<link rel="stylesheet" charset="utf-8" type="text/css" href="http://'.$path.'bootstrap/css/bootstrap-responsive.min.css"/>';
    }

    /**
     * 引用jquery的快捷方法
     */
    public static function jQuery(){
        $path = $_SERVER['SERVER_NAME'].strtr($_SERVER['SCRIPT_NAME'],array('index.php'=>'')).'Public/';
        echo '<script type="text/javascript" charset="utf-8" src="http://'.$path.'js/jQuery/jquery-1.7.1.js"></script>';
    }
    /**
     * 引用bootstrap的轮播插件
     */
    public static function bootstrapCarousel(){
        $path = $_SERVER['SERVER_NAME'].strtr($_SERVER['SCRIPT_NAME'],array('index.php'=>'')).'Public/';
        echo '<script type="text/javascript" charset="utf-8" src="http://'.$path.'bootstrap/js/bootstrap-carousel.js"></script>';
    }
    /**
     * 引用轮播插件
     */
    public static function camera(){
        $path = $_SERVER['SERVER_NAME'].strtr($_SERVER['SCRIPT_NAME'],array('index.php'=>'')).'Public/';
        echo <<<EOT
            <link rel='stylesheet'  href='http://{$path}camera/css/camera.css' type='text/css' media='all'>
            <script type='text/javascript' src='http://{$path}camera/scripts/jquery.min.js'></script>
            <script type='text/javascript' src='http://{$path}camera/scripts/jquery.mobile.customized.min.js'></script>
            <script type='text/javascript' src='http://{$path}camera/scripts/jquery.easing.1.3.js'></script>
            <script type='text/javascript' src='http://{$path}camera/scripts/camera.js'></script>
EOT;
    }
    /**
     * 登录页面用的
     */
    public static function login(){
        $path = $_SERVER['SERVER_NAME'].strtr($_SERVER['SCRIPT_NAME'],array('index.php'=>'')).'Public/';
        echo <<<EOT
            <link rel='stylesheet'  href='http://{$path}login/bootstrap-2.2.0.css' type='text/css'>
EOT;
    }
    /**
     * 登录页面用的，兼容IE9的js
     */
    public static function loginForIe9(){
        $path = $_SERVER['SERVER_NAME'].strtr($_SERVER['SCRIPT_NAME'],array('index.php'=>'')).'Public/';
        echo <<<EOT
            <script type='text/javascript' src='http://{$path}login/html5.js'></script>
EOT;
    }

    public static function resourcePath(){
        return 'http://'.$_SERVER['SERVER_NAME'].strtr($_SERVER['SCRIPT_NAME'],array('index.php'=>'')).'Public/';
    }

    /**
     * jQuery 图片上传插件
     */
    public function uploadify(){
        $path = self::resourcePath();
        echo <<<EOT
            <script type='text/javascript' src='{$path}uploadify/jquery.uploadify.min.js'></script>
            <link rel='stylesheet'  href='{$path}uploadify/uploadify.css' type='text/css' media='all'>
EOT;
    }

    /**
     * jquery 插件
     * masonry  瀑布流产检
     */
    public function masonry(){
        $path = self::resourcePath();
        echo <<<EOT
            <script type='text/javascript' src='{$path}js/masonry/jquery.masonry.js'></script>
            <script type='text/javascript' src='{$path}js/masonry/jquery.infinitescroll.js'></script>
EOT;
    }
}