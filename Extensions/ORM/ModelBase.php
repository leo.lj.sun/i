<?php
namespace ORM;
use ORM\SyntaxTree;
use ReflectionClass;
use Exception;
use ORM\Driver\DB;

/*
 * 模型基类
 * todo  在insert和upodate的时候把入库的内容转成实体
*/
abstract class ModelBase
{
    const GET_BY = 'getBy';
    const DELETE_BY = 'deleteBy';

    const GET = 'get';
    const SET = 'set';

    private static $cache = array(); //缓存生命周期内的查询结果

//    private $sqlArr = array();
    private $syntaxTree = null;

    private $className = null;

    private $properties = array();

    private $namespaceArr = array(); //模型对应的命名空间 key为模型名，value为命名空间，仅在多表查询的时候使用

    private $toArray = false;

    abstract function getDefaultDB();

    public function __construct(array $properties = array())
    {
        $this->syntaxTree = new SyntaxTree\SqlSyntaxTree();
        $this->className = get_called_class();
        $this->getAllProperties();
        $this->initProperties($properties);
        $this->setTableName();
    }

    /**
     * 给模型的不同属性赋值
     * @param array $properties
     */
    private function initProperties(array $properties = array())
    {
        foreach ($properties as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * 获取表名，分表需要重写此方法
     * @param $subTableBasis 分表依据的参数
     * @return string
     */
    protected function getTableName($table = null, $subTableBasis = null)
    {
        if (null === $table) {
            $arr = explode('\\', get_called_class());
            return array_pop($arr);
        } else {
            return $table;
        }
    }

    public function where($where = null)
    {
        if (array() != $this->syntaxTree->where) {
            $this->syntaxTree->setWhere($this->syntaxTree->whereType['and']);
        }
        $this->syntaxTree->setWhere($where);
        return $this;
    }

    public function orWhere($where = null)
    {
        $this->syntaxTree->setWhere($this->syntaxTree->whereType['or']);
        $this->syntaxTree->setWhere($where);
        return $this;
    }

    /**
     *
     * @return ModelBase
     * @throws \Exception
     */
    public function limit()
    {
        $args = func_get_args();
        if (count($args) == 1 && is_numeric($args[0])) {
            $this->syntaxTree->limit = $args;
        } elseif (count($args) == 2 && is_numeric($args[0]) && is_numeric($args[1])) {
            $this->syntaxTree->limit = $args;
        } else {
            throw new Exception('limit params error');
        }
        return $this;
    }

    /**
     *
     * @return ModelBase
     */
    public function orderBy($str)
    {
        $this->syntaxTree->setOrderBy($str);
        return $this;
    }

    public function desc()
    {
        $this->syntaxTree->desc();
        return $this;
    }


    /**
     * @return int 响应行数
     */
    public function update()
    {
        $args = func_get_args();
        $this->syntaxTree->fields = $args;
        $this->syntaxTree->operateType = OperateTypeEnum::UPDATE;
        $args = $this->exec();
        return DB\MyPdo::exec($args['dbInfo'], $args['sql'], $args['params']);
    }

    /**
     * @return int 响应行数
     */
    public function insert()
    {
        $args = func_get_args();
        if (empty($args)) {
            throw new Exception('insert fields cannot be empty');
        }
        $this->syntaxTree->fields = $args;
        $this->syntaxTree->operateType = OperateTypeEnum::INSERT;
        $args = $this->exec();
        return DB\MyPdo::exec($args['dbInfo'], $args['sql'], $args['params']);
    }

    public static function rowCount()
    {
        return DB\MyPdo::rowCount();
    }

    public function getInsertId()
    {
        return DB\MyPdo::getInsertId();
    }

    /**
     * @return int 响应行数
     */
    public function delete()
    {
        $this->syntaxTree->operateType = OperateTypeEnum::DELETE;
        $args = $this->exec();
        return DB\MyPdo::exec($args['dbInfo'], $args['sql'], $args['params']);
    }

    public function setTableName($table = null, $subTableBasis = null)
    {
        $this->syntaxTree->source = $this->getTableName($table, $subTableBasis);
        return $this;
    }

    /**
     * 执行sql的方法
     * @return mixed
     */

    private function exec()
    {
        $dbInfo = $this->getDefaultDB();
        if ($this->syntaxTree->isJoin) {
            $this->analysisNamespace($this->className);
        }
        $driver = DriverAdapter::getInstance($dbInfo, $this->syntaxTree);
        $sql = $driver->getSqlString();
        if (isset(self::$cache[md5($sql . $this->toArray)])) {
//            return self::$cache[md5($sql . $this->toArray)];
        }
        if (TRUE === DISPLAY_SQL) {
//            self::$sqlArr[time()] = $sql;
            echo $sql, '<br />';
        }
        $params = $driver->getSqlParams($this->properties);
        $res['sql'] = $sql;
        $res['params'] = $params;
        $res['dbInfo'] = $dbInfo;
        return $res;
    }

    /**
     * 回调函数可操作单个对象
     * @param function $callback
     * @return array|null|ModelBase
     */
    public function fetchAll($callback = null)
    {
        $this->syntaxTree->operateType = OperateTypeEnum::SELECT;
        $args = $this->exec();
        $data = DB\MyPdo::fetchAll($args['dbInfo'], $args['sql'], $args['params']);
        if ($this->toArray) {
            $result = $this->processDataForManyToArray($data, $callback);
        } else {
            $result = $this->processDataForManyToObject($data, $callback);
        }
        self::$cache[md5($args['sql'] . $this->toArray)] = $result; //缓存生命周期内的查询结果
        return $result;
    }

    private function processDataForManyToArray($data, $callback = null)
    {
        $result = array();
        foreach ($data as $item) {
            $result[] = $this->processDataForRowToArray($item);
        }
        return $result;
    }

    private function processDataForRowToArray($data, $callback = null)
    {
        $result = array();
        if ($this->syntaxTree->isJoin) {
            foreach ($data as $key => $value) {
                $keyArr = explode('_', $key);
                $model = array_shift($keyArr);
                //组出字段名,防止有字段名带下划线
                $field = implode('_', $keyArr);
                $result[$model][$field] = $value;
            }
        } else {
            $result = $data;
        }

        is_callable($callback) && $callback($result);
        return $result;
    }

    /**
     * 回调函数返回模型对象
     * @param null $callback
     * @return array|null|ModelBase
     */
    public function fetchRow($callback = null)
    {
        $this->syntaxTree->operateType = OperateTypeEnum::SELECT;
        $args = $this->exec();
        $data = DB\MyPdo::fetchRow($args['dbInfo'], $args['sql'], $args['params']);
        if ($this->toArray) {
            $result = $this->processDataForRowToArray($data);
        } else {
            $result = $this->processDataForRowToObject($data, $callback);
        }
        self::$cache[md5($args['sql'] . $this->toArray)] = $result; //缓存生命周期内的查询结果
        return $result;
    }

    /**
     * @param $data
     * @param null $callback
     * @return 返回数组包含的对象
     */
    private function processDataForRowToObject($data, $callback = null)
    {
        if ($this->syntaxTree->isJoin) {
            $arr = $this->processDataForRowToArray($data);
            foreach ($arr as $model => $item) {
                $obj = new $this->className();
                if ($this->className != $this->namespaceArr[$model]) {
                    $obj->$model = new $this->namespaceArr[$model]($item);
                } else {
                    $obj->initProperties($item);
                }
            }
            $result = $obj;
        } else {
            $result = new $this->className($data);
        }
        is_callable($callback) && $callback($result);
        return $result;
    }

    /**
     * 处理多条数据
     * @param $data
     * @param $callback
     * @return 返回数组包含的对象
     */
    private function processDataForManyToObject($data, $callback = null)
    {
        $result = array();
        foreach ($data as $row) {
            $item = $this->processDataForRowToObject($row, $callback);
            $result[] = $item;
        }
        return $result;
    }

    /**
     * @param $join
     * @return ModelBase
     */
    public function join($join)
    {
        $this->syntaxTree->isJoin = true;
        $this->syntaxTree->setJoin($this->syntaxTree->joinType['innerJoin']);
        $this->syntaxTree->setJoin(array_pop(explode('\\', $join)));
        $this->analysisNamespace($join);
        return $this;
    }

    public function leftJoin($leftJoin)
    {
        $this->syntaxTree->isJoin = true;
        $this->syntaxTree->setJoin($this->syntaxTree->joinType['leftJoin']);
        $this->syntaxTree->setJoin(array_pop(explode('\\', $leftJoin)));
        $this->analysisNamespace($leftJoin);
        return $this;
    }

    public function rightJoin($rightJoin)
    {
        $this->syntaxTree->isJoin = true;
        $this->syntaxTree->setJoin($this->syntaxTree->joinType['rightJoin']);
        $this->syntaxTree->setJoin(array_pop(explode('\\', $rightJoin)));
        $this->analysisNamespace($rightJoin);
        return $this;
    }

    public function on($on)
    {
        $this->syntaxTree->isJoin = true;
        $this->syntaxTree->setJoin('on');
        $this->syntaxTree->setJoin($on);
        $this->syntaxTree->setJoin(',');
        return $this;
    }

    /**
     *
     * @param $page
     * @param int $pageSize
     */
    public function page($page = 1, $pageSize = 20)
    {
        $this->limit((--$page) * $pageSize, $pageSize);
        return $this;
    }

    public function isExists()
    {
        $this->syntaxTree->isExists = true;
        $data = $this->columns('*')->fetchRow();
        return $data->result == 1 ? true : false;
    }

    public function count($field = '*')
    {
        return $this->fetchValue("count({$field})");
    }

    /**
     * 获取一条数据中指定字段值
     * @param $field
     * @return mixed
     */
    public function fetchValue($field)
    {
        $data = $this->columns($field)->fetchRow();
        return $data->$field;
    }

    /**
     * 设置要获取的字段
     * @return ModelBase
     * @throws \Exception
     */
    public function columns()
    {
        $args = func_get_args();
        if (count($args) == 0) {
            throw new Exception("Columns can not be empty or *");
        }
        $this->syntaxTree->fields = array_merge($this->syntaxTree->fields, $args);
        return $this;
    }

    /**
     * @params string $field  Id
     * in的参数必须放到PDO预执行的sql里面去
     */
    public function in($field, array $arr)
    {
        $this->syntaxTree->inField = $field;
        $this->syntaxTree->in = $arr;
        return $this;
    }

    /**
     * @params string $field  Id
     * in的参数必须放到PDO预执行的sql里面去
     */
    public function notIn($field, array $arr)
    {
        $this->syntaxTree->notInField = $field;
        $this->syntaxTree->notIn = $arr;
        return $this;
    }

    protected function getDriver()
    {
        $dbInfo = $this->getDefaultDB();
        return $dbInfo;
    }

    public function beginTransaction()
    {
        $dbInfo = $this->getDefaultDB();
        DB\MyPdo::beginTransaction($dbInfo);
    }

    public function commit()
    {
        DB\MyPdo::commit();
    }

    public function rollBack()
    {
        DB\MyPdo::rollBack();
    }

    public function query($sql)
    {
        $args = func_get_args();
        array_shift($args);
        $dbInfo = $this->getDefaultDB();
        $data = DB\MyPdo::query($dbInfo, $sql, $args);
        return $data;
    }

    public function addParams(array $params)
    {
        $this->properties = array_merge($this->properties, $params);
        return $this;
    }

    /**
     * 魔术方法
     * @param $name
     * @param $arguments
     * @return $this
     */
    public function __call($name, $arguments)
    {
        if (strpos($name, self::GET_BY) == 0 && strstr($name, self::GET_BY)) {
            $key = substr($name, strlen(self::GET_BY));
            $this->$key = $arguments[0];
            $data = $this->where($key . '=:' . $key)->fetchAll();
            return $data;
        } elseif (strpos($name, self::DELETE_BY) == 0 && strstr($name, self::DELETE_BY)) {
            $key = substr($name, strlen(self::DELETE_BY));
            $this->$key = $arguments[0];
            return $this->where($key . '=:' . $key)->delete();
        } elseif (substr($name, 0, strlen(self::SET)) == self::SET) {
            $key = substr($name, strlen(self::SET));
            $this->$key = $arguments[0];
            return $this;
        } elseif (substr($name, 0, strlen(self::GET)) == self::GET) {
            $key = substr($name, strlen(self::GET));
            return $this->$key;
        }
    }

    /**
     * 读取model的公共属性
     */
    private function getAllProperties()
    {
        $class = new ReflectionClass($this->className);
        $properties = $class->getDefaultProperties();
        foreach ($properties as $name => $value) {
            $property = $class->getProperty($name);
            if ($property->getDeclaringClass()->getName() == $class->getName()
                && $property->isPublic()
            ) {
                $this->properties[$name] = $value;
            }
        }
    }

    public function __set($name, $value)
    {
        $this->properties[$name] = $value;
        return $this;
    }

    public function __get($name)
    {
        return $this->properties[$name];
    }

    public function toArray()
    {
        $this->toArray = true;
        return $this;
    }

    /**
     * 静态实例化的方法
     * @param array $properties
     * @return mixed
     */
    public static function getInstance(array $properties = array())
    {
        $class = get_called_class();
        $instance = new $class();
        $instance->syntaxTree = new SyntaxTree\SqlSyntaxTree();
        $instance->getAllProperties();
        $instance->className = $class;
        $instance->properties = $properties;
        $instance->setTableName();
        return $instance;
    }

    /**
     * 分析模型的命名空间
     * @param $model
     */
    private function analysisNamespace($model)
    {
        if (strrpos($model, '\\') > 0) {
            $namespace = explode('\\', $model);
            $this->namespaceArr[array_pop($namespace)] = $model;
        } else {
            $namespace = explode('\\', $this->className);
            array_pop($namespace);
            $this->namespaceArr[$model] = $namespace . '\\' . $model;
        }
    }
}