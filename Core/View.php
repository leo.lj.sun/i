<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Sun
 * Date: 13-9-10
 * Time: 上午11:09
 * To change this template use File | Settings | File Templates.
 */
namespace i\Core;
use Exception;

class View
{

    //todo 页面控件
    private static $_var = array();

    public static $layout = true;

    // todo 设置不同的布局文件

    /**
     * 设置变量
     * @param $varName
     * @param $var
     */
    public static function setVar($varName, $var)
    {
        self::$_var[$varName] = $var;
    }

    /**
     * 获取变量
     * @return array
     */
    public static function getVars()
    {
        return self::$_var;
    }

    /**
     * 思路：先获取视图地址，然后组出需要在页面使用的变量
     * 然后读取视图到缓冲区，赋给一个变量
     * 最后假如需要使用布局，在布局的指定地方输出视图内容
     * 假如不使用布局，那么直接输出视图内容
     * @param null $view
     * @param null $data
     * @throws \Exception
     */
    public static function render($view = null, $data = null)
    {
        $viewPath = self::getViewPath($view); //获取视图地址
        self::mergeVars($data); //合并变量
        extract(self::$_var); //添加变量
        ob_start(); //开启缓冲区
        $viewFile = APP_PATH .DS.'Protected'.DS. 'Views' . DS . $viewPath . EXT; //加载视图文件
        if (file_exists($viewFile)) {
            require_once $viewFile; //获取视图内容
            $content = ob_get_contents();
            ob_clean(); //清除缓冲区内容
        } else {
            throw new Exception($viewFile . ' is not found.');
        }
        if (self::$layout) {
            self::layout($content);
        } else {
            echo $content;//不使用布局文件的情况
        }
        ob_end_flush(); //清除缓冲区内容并且关闭缓冲区
    }

    /**
     * 加载布局文件
     * @param $content  此变量名称不能边，变量值为View的内容，在layout文件输出
     * @throws \Exception
     */
    private static function layout($content)
    {
        $layout = APP_PATH .DS.'Protected'.DS. 'Views' . DS . 'Layout' . DS . 'main' . EXT;
        if (file_exists($layout)) {
            extract(self::$_var); //添加变量
            require_once $layout;
        } else {
            throw new Exception($layout . ' is not found.');
        }
    }

    /**
     * 合并action里面set的变量和render后面的变量
     * @param null $data
     */
    private static function mergeVars($data = null)
    {
        (null !== $data) && self::$_var = array_merge(self::$_var, $data);
    }

    /**
     * 解析视图路径
     * 有三种情况
     * 1.不传参数，视图文件与当前action名字相同
     * 2.‘index’
     * 3.‘controller/index’
     * @param $view
     * @return string
     * @throws \Exception
     */
    private static function getViewPath($view)
    {
        $viewArr = explode('/', $view);
        switch (count($viewArr)) {
            case 1:
                $viewFolder = Route::getController();
                $view = empty($viewArr[0]) ? Route::getAction() : $view;
                break;
            case 2;
                $viewFolder = $viewArr[0];
                $view = $viewArr[1];
                break;
            default:
                throw new Exception('render 方法参数错误');
                break;
        }
        return $viewFolder . DS . ucfirst($view);
    }

    /**
     * 创建Url 这个东西是不是要卸载视图类里面呢
     */
    public function createUrl(){

    }
}
