<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Sun
 * Date: 13-9-10
 * Time: 上午10:56
 * To change this template use File | Settings | File Templates.
 */
namespace i\Core;
use i\Core\View;
use i\Core\Route;
class Controller
{
    public $runAction = null;
    public $runController = null;

    public function __construct(){
        $this->runAction = Route::getAction();
        $this->set('runAction',Route::getAction());
        $this->runController = Route::getController();
        $this->set('runController',Route::getController());
    }

    public function layout($isLayout = true)
    {
        View::$layout = $isLayout;
    }

    public function render($view = null, $data = null)
    {
        View::render($view, $data);
    }

    public function set($varName, $var)
    {
        View::setVar($varName, $var);
    }

    public function beforeAction()
    {
    }

    public function afterAction()
    {
    }

    /**
     * 返回json
     * todo 刷新缓冲区输出和 echo 输出有什么区别
     * @param $mix
     */
    public function writeJson($mix){
        echo json_encode($mix);
        die;
    }

    /**
     * 重定向
     * @param $action
     */
    public function redirect($str){
        $location = '';
        if(strpos($str,'/') ===false){
            $controller = Route::getController();
            $location = $controller.'/'.$str;
        }else{
            $location = $str;
        }
       // $URL = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'].'/'.$location;
       $URL = 'http://'.$_SERVER['HTTP_HOST'].'/'.$location;
	 header("location: {$URL}",TRUE,301);
    }

    protected function getConfig($key){
        return I::getConfig($key);
    }
}
